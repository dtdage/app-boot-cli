package io.gitee.dtdage.app.boot.starter.web.security.context;

import java.lang.annotation.*;

/**
 * 鉴权注解
 *
 * @author WFT
 * @since 2024/4/15
 */
@Inherited
@Documented
@Target(value = ElementType.METHOD)
@Retention(value = RetentionPolicy.RUNTIME)
public @interface PreAuthorize {

    /**
     * @return 权限标识列表
     */
    String[] value() default {};

}
