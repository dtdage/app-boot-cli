package io.gitee.dtdage.app.boot.starter.web.security.exception;

import io.gitee.dtdage.app.boot.starter.web.common.context.HttpStatus;

/**
 * 401 异常
 *
 * @author WFT
 * @since 2022/1/4
 */
public class UnauthorizedException extends BaseSecurityException {

    public UnauthorizedException() {
        this("Full authentication is required to access this resource.");
    }

    /**
     * @noinspection WeakerAccess
     */
    public UnauthorizedException(String message) {
        super(message, HttpStatus.UNAUTHORIZED);
    }

}
