package io.gitee.dtdage.app.boot.starter.web.security.context;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.io.Serializable;

/**
 * 访问令牌
 *
 * @author WFT
 * @since 2024/4/15
 */
@Getter
@AllArgsConstructor
public class AccessToken implements Serializable {

    /**
     * 访问令牌
     */
    private final String token;

    /**
     * 用户主体
     */
    private final Principal principal;

    /**
     * 失效时间
     */
    private final Integer refresh;

}