package io.gitee.dtdage.app.boot.starter.web.security.context;

import io.gitee.dtdage.app.boot.starter.common.context.BeanFactory;
import io.gitee.dtdage.app.boot.starter.web.security.service.AccessTokenService;
import org.springframework.stereotype.Component;

/**
 * 访问令牌工厂
 *
 * @author WFT
 * @since 2024/4/15
 */
@Component
public class TokenFactory extends BeanFactory<String, AccessTokenService> {

    @Override
    protected IllegalArgumentException illegalArgumentException() {
        return new IllegalArgumentException("访问令牌生成规则不存在");
    }

}