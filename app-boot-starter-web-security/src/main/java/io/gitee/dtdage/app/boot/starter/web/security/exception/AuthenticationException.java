package io.gitee.dtdage.app.boot.starter.web.security.exception;

import io.gitee.dtdage.app.boot.starter.web.common.context.HttpStatus;

/**
 * 407 异常
 *
 * @author WFT
 * @since 2022/1/4
 */
public class AuthenticationException extends BaseSecurityException {


    public AuthenticationException() {
        this("Please replace with a new certificate. Current certificate has expired.");
    }

    /**
     * @noinspection WeakerAccess
     */
    public AuthenticationException(String message) {
        super(message, HttpStatus.PROXY_AUTHENTICATION_REQUIRED);
    }

}
