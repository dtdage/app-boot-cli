package io.gitee.dtdage.app.boot.starter.web.security.handler;

import io.gitee.dtdage.app.boot.starter.web.common.handler.InterceptorHandler;
import io.gitee.dtdage.app.boot.starter.web.security.context.SecurityContext;
import io.gitee.dtdage.app.boot.starter.web.security.context.TokenFactory;
import io.gitee.dtdage.app.boot.starter.web.security.exception.BaseSecurityException;
import io.gitee.dtdage.app.boot.starter.web.security.service.TokenStorageService;
import io.gitee.dtdage.app.boot.starter.web.security.utils.SecurityUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Collections;
import java.util.List;

/**
 * 认证拦截器
 *
 * @author WFT
 * @since 2024/4/15
 */
@Component
@RequiredArgsConstructor
public class GrantInterceptorHandler implements InterceptorHandler {

    private final TokenStorageService storageService;
    private final TokenFactory factory;

    @Override
    public List<String> getPathPatterns() {
        return Collections.singletonList("/**");
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
        //  构建权限上下文
        SecurityContext context = new SecurityContext();
        //  获取客户端编号/令牌生成规则
        context.setClientId(request.getHeader("client-id"));
        context.setScene(request.getHeader("client-scene"));
        context.setRule(request.getHeader("token-rule"));
        try {
            //  获取访问令牌
            context.setToken(this.storageService.getAccessToken(request));
            //  获取当前用户主体信息
            context.setPrincipal(this.factory.getInstance(context.getRule()).getPrincipal(context));
        } catch (BaseSecurityException e) {
            context.setException(e);
        } finally {
            SecurityUtil.set(context);
        }
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object o, ModelAndView view) {
        SecurityUtil.clear();
    }

}
