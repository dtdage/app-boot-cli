package io.gitee.dtdage.app.boot.starter.data.redis.context;

import io.gitee.dtdage.app.boot.starter.common.context.BeanFactory;
import io.gitee.dtdage.app.boot.starter.data.redis.handler.KeyExpirationEventHandler;
import org.springframework.stereotype.Component;

/**
 * Key过期事件处理器工厂
 *
 * @author WFT
 * @since 2024/4/14
 */
@Component
public class KeyExpireHandlerFactory extends BeanFactory<String, KeyExpirationEventHandler> {


}
