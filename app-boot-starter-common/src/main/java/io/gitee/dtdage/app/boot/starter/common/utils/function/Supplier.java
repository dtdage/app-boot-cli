package io.gitee.dtdage.app.boot.starter.common.utils.function;

/**
 * @author WFT
 * @since 2024/3/30
 */
@FunctionalInterface
public interface Supplier<T> {

    /**
     * 获取一个对象
     *
     * @return {@link Object}
     * @throws Exception java.lang.Exception
     */
    T get() throws Exception;

}
