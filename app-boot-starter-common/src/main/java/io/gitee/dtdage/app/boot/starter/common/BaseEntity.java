package io.gitee.dtdage.app.boot.starter.common;

import java.io.Serializable;

/**
 * @author WFT
 * @since 2024/4/14
 */
public interface BaseEntity<T extends Serializable> extends BaseBean<T> {

    /**
     * 设置主键
     *
     * @param id {@link Serializable}
     * @noinspection unused
     */
    void setId(T id);

}
