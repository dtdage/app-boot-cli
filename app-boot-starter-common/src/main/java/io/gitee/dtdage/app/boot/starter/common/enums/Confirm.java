package io.gitee.dtdage.app.boot.starter.common.enums;

import io.gitee.dtdage.app.boot.starter.common.BaseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 询问标记
 *
 * @author WFT
 * @since 2024/4/14
 */
@Getter
@AllArgsConstructor
public enum Confirm implements BaseEnum<Integer> {

    /**
     * 同意
     */
    yes(1),

    /**
     * 拒绝
     */
    not(2);

    private final Integer value;

}