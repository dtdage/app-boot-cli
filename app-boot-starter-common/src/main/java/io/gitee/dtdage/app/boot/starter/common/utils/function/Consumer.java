package io.gitee.dtdage.app.boot.starter.common.utils.function;

/**
 * @author WFT
 * @since 2024/3/30
 */
@FunctionalInterface
public interface Consumer<T> {

    /**
     * Performs this operation on the given argument.
     *
     * @param t the input argument
     * @throws Exception java.lang.Exception
     */
    void accept(T t) throws Exception;

}
