package io.gitee.dtdage.app.boot.starter.common;

/**
 * 客户端类型
 *
 * @author WFT
 * @since 2024/4/18
 */
public interface Terminal extends BaseEnum<Integer> {


}
