package io.gitee.dtdage.app.boot.starter.common.utils.function;

/**
 * @author WFT
 * @since 2024/3/30
 */
@FunctionalInterface
public interface Runnable {

    /**
     * 执行任务
     *
     * @throws Exception java.lang.Exception
     */
    void run() throws Exception;

}
