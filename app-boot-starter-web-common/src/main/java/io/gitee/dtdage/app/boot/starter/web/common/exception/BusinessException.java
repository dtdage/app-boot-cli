package io.gitee.dtdage.app.boot.starter.web.common.exception;

import lombok.Getter;

/**
 * 业务异常
 *
 * @author WFT
 * @since 2024/4/15
 */
@Getter
public class BusinessException extends RuntimeException {

    /**
     * Http状态码
     */
    private final int code;

    /**
     * 数据
     */
    private final Object data;

    public BusinessException(String message, int code) {
        this(message, code, null);
    }

    /**
     * @noinspection WeakerAccess
     */
    public BusinessException(String message, int code, Object data) {
        super(message);
        this.code = code;
        this.data = data;
    }

}
