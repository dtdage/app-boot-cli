package io.gitee.dtdage.app.boot.starter.web.common.exception.handler;

import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.stereotype.Component;

/**
 * @author WFT
 * @since 2024/3/30
 */
@Component
public class HttpMessageNotReadableExceptionHandler extends BaseParameterExceptionHandler<Class<HttpMessageNotReadableException>> {

    @Override
    public Class<HttpMessageNotReadableException> getId() {
        return HttpMessageNotReadableException.class;
    }

}
