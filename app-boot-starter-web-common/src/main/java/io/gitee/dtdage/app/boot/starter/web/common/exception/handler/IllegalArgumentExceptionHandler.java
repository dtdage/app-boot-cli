package io.gitee.dtdage.app.boot.starter.web.common.exception.handler;

import org.springframework.stereotype.Component;

/**
 * @author WFT
 * @since 2024/3/30
 */
@Component
public class IllegalArgumentExceptionHandler extends BaseParameterExceptionHandler<Class<IllegalArgumentException>> {

    @Override
    public Class<IllegalArgumentException> getId() {
        return IllegalArgumentException.class;
    }

}
