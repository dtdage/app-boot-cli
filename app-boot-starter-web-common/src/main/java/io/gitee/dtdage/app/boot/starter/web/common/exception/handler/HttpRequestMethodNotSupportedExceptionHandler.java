package io.gitee.dtdage.app.boot.starter.web.common.exception.handler;

import io.gitee.dtdage.app.boot.starter.web.common.context.HttpStatus;
import io.gitee.dtdage.app.boot.starter.web.common.context.Response;
import io.gitee.dtdage.app.boot.starter.web.common.handler.ExceptionHandler;
import org.springframework.stereotype.Component;
import org.springframework.web.HttpRequestMethodNotSupportedException;

/**
 * @author WFT
 * @since 2024/3/30
 */
@Component
public class HttpRequestMethodNotSupportedExceptionHandler implements ExceptionHandler<Class<HttpRequestMethodNotSupportedException>> {

    @Override
    public <E extends Throwable> Response<?> resolve(E exception) {
        return new Response<>(HttpStatus.METHOD_NOT_ALLOWED, "Request method not supported!");
    }

    @Override
    public Class<HttpRequestMethodNotSupportedException> getId() {
        return HttpRequestMethodNotSupportedException.class;
    }

}
