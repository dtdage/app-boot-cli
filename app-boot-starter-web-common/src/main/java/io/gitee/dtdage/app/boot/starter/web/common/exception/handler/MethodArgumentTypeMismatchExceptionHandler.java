package io.gitee.dtdage.app.boot.starter.web.common.exception.handler;

import org.springframework.stereotype.Component;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

/**
 * @author WFT
 * @since 2024/3/30
 */
@Component
public class MethodArgumentTypeMismatchExceptionHandler extends BaseParameterExceptionHandler<Class<MethodArgumentTypeMismatchException>> {

    @Override
    public Class<MethodArgumentTypeMismatchException> getId() {
        return MethodArgumentTypeMismatchException.class;
    }

}
