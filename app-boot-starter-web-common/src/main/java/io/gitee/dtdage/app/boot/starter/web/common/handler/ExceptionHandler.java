package io.gitee.dtdage.app.boot.starter.web.common.handler;

import io.gitee.dtdage.app.boot.starter.common.BaseBean;
import io.gitee.dtdage.app.boot.starter.web.common.context.Response;

/**
 * 异常处理器接口
 *
 * @author WFT
 * @since 2024/4/14
 */
public interface ExceptionHandler<T extends Class<? extends Throwable>> extends BaseBean<T> {

    /**
     * 解决异常
     *
     * @param exception java.lang.Exception
     * @param <E>       {@link Exception}
     * @return {@link Response}
     */
    <E extends Throwable> Response<?> resolve(E exception);

}
