package io.gitee.dtdage.app.boot.starter.web.common.context;

import io.gitee.dtdage.app.boot.starter.common.utils.JsonUtil;
import lombok.Getter;

import java.io.Serializable;

/**
 * @author WFT
 * @since 2024/4/14
 */
@Getter
public class Response<T> implements Serializable {

    private int code;

    private String msg;

    private T data;

    public Response(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public Response(int code, String msg, T data) {
        this(code, msg);
        this.data = data;
    }

    @Override
    public String toString() {
        return JsonUtil.getInstance().toJson(this);
    }
}
