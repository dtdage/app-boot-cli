package io.gitee.dtdage.app.boot.starter.data.storage.context;

import io.gitee.dtdage.app.boot.starter.common.BaseEntity;
import io.gitee.dtdage.app.boot.starter.data.storage.enums.Supplier;

/**
 * 存储桶配置接口
 *
 * @author WFT
 * @since 2024/4/14
 */
public interface Configure extends BaseEntity<Long> {

    /**
     * @return 密钥编号
     */
    String getSecretId();

    /**
     * @return 密钥
     */
    String getSecretKey();

    /**
     * @return 区域名称
     */
    String getBucketRegion();

    /**
     * @return 存储桶的名称
     */
    String getBucketName();

    /**
     * @return 服务提供商
     */
    Supplier getSupplier();

    /**
     * @return 代理路径
     */
    String getProxyPath();

}
