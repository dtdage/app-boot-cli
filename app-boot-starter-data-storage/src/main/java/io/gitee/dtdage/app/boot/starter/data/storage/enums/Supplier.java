package io.gitee.dtdage.app.boot.starter.data.storage.enums;

import io.gitee.dtdage.app.boot.starter.common.BaseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author WFT
 * @since 2024/4/14
 */
@Getter
@AllArgsConstructor
public enum Supplier implements BaseEnum<Integer> {

    /**
     * 腾讯云
     *
     * @noinspection SpellCheckingInspection
     */
    tencent_cloud(1),

    /**
     * 阿里云
     *
     * @noinspection SpellCheckingInspection
     */
    alipay_cloud(2),

    /**
     * 七牛云
     *
     * @noinspection SpellCheckingInspection
     */
    qiniu_cloud(3);

    private final Integer value;

}
