package io.gitee.dtdage.app.boot.starter.data.storage.service;

import io.gitee.dtdage.app.boot.starter.common.BaseBean;
import io.gitee.dtdage.app.boot.starter.data.storage.enums.Supplier;

import java.io.InputStream;

/**
 * 对象存储接口
 *
 * @author WFT
 * @since 2024/4/14
 */
public interface StorageService extends BaseBean<Supplier> {

    /**
     * 删除文件
     *
     * @param path 文件访问地址
     * @throws Exception 异常
     */
    void delete(String path) throws Exception;

    /**
     * 文件上传
     *
     * @param stream   输入流
     * @param filename 文件名称
     * @param suffix   后缀名
     * @return 访问地址
     * @throws Exception 异常
     */
    String upload(InputStream stream, String filename, String suffix) throws Exception;

}
