package io.gitee.dtdage.app.boot.starter.data.storage.service;

import io.gitee.dtdage.app.boot.starter.data.storage.context.Configure;
import io.gitee.dtdage.app.boot.starter.data.storage.enums.Supplier;

/**
 * 存储桶配置增删改查接口
 *
 * @author WFT
 * @since 2024/4/14
 */
public interface ConfigureService<T extends Configure> {

    /**
     * 获取存储桶配置
     *
     * @param supplier 服务提供商
     * @return {@link Configure}
     */
    T getConfigure(Supplier supplier);

}
