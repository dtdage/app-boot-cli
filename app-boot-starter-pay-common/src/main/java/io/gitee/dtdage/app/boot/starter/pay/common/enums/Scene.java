package io.gitee.dtdage.app.boot.starter.pay.common.enums;

import io.gitee.dtdage.app.boot.starter.common.BaseEnum;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * 支付场景
 *
 * @author WFT
 * @since 2024/4/9
 */
@Getter
@RequiredArgsConstructor
public enum Scene implements BaseEnum<Integer> {

    /**
     * 微信App支付
     */
    wx_app(1),

    /**
     * 微信H5支付
     */
    wx_h5(2),

    /**
     * 微信公众号,小程序支付
     *
     * @noinspection SpellCheckingInspection
     */
    wx_jsapi(3),

    /**
     * 微信Pc网站支付
     */
    wx_pc(4),

    /**
     * 支付宝App支付
     */
    zfb_app(5),

    /**
     * 支付宝H5支付
     */
    zfb_h5(6),

    /**
     * 支付宝小程序支付
     *
     * @noinspection SpellCheckingInspection
     */
    zfb_jsapi(7),

    /**
     * 支付宝Pc网站支付
     */
    zfb_pc(8);

    private final Integer value;

}
