package io.gitee.dtdage.app.boot.starter.pay.common.context;

import io.gitee.dtdage.app.boot.starter.common.BaseEntity;
import io.gitee.dtdage.app.boot.starter.common.Terminal;

/**
 * 配置接口
 *
 * @author WFT
 * @since 2024/4/14
 */
public interface ConfigBean extends BaseEntity<Long> {

    /**
     * 此编号用于区分客户端,例如：微信公众号,微信小程序,支付宝小程序,抖音小程序....
     *
     * @return 客户端编号
     */
    String getClientId();

    /**
     * 该字段用于区分用户类型，例如：平台管理员,平台商户,普通用户...
     *
     * @return 客户端代理编号
     */
    @SuppressWarnings("unused")
    Terminal getTerminalId();

    /**
     * @return 第三方应用编号
     */
    String getAppId();

    /**
     * @return 支付通知API地址
     */
    String getPayNotifyPath();

}
