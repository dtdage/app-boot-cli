package io.gitee.dtdage.app.boot.starter.pay.common.context;

import io.gitee.dtdage.app.boot.starter.common.BaseEntity;

/**
 * 退款参数接口
 *
 * @author WFT
 * @since 2024/4/14
 */
public interface RefundBean extends BaseEntity<Long> {

    /**
     * @return 商户交易编号
     */
    String getTradeNo();

    /**
     * @return 原订单金额
     */
    Integer getTradePrice();

    /**
     * @return 退款金额
     */
    Integer getRefundPrice();

    /**
     * @return 客户端编号
     */
    String getClientId();

}