package io.gitee.dtdage.app.boot.starter.pay.wx.service;

import io.gitee.dtdage.app.boot.starter.pay.wx.context.ConfigBean;

/**
 * @author WFT
 * @since 2024/4/14
 * @noinspection WeakerAccess
 */
public interface ConfigureService<T extends ConfigBean> extends io.gitee.dtdage.app.boot.starter.pay.common.service.ConfigureService<T> {


}
