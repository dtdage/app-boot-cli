package io.gitee.dtdage.app.boot.starter.pay.zfb.service.impl;

import com.alipay.api.request.AlipayTradePagePayRequest;
import io.gitee.dtdage.app.boot.starter.pay.common.context.TradeBean;
import io.gitee.dtdage.app.boot.starter.pay.common.enums.Scene;
import io.gitee.dtdage.app.boot.starter.pay.zfb.context.ConfigBean;
import io.gitee.dtdage.app.boot.starter.pay.zfb.service.BaseTradeService;
import io.gitee.dtdage.app.boot.starter.pay.zfb.utils.ZfbSDKUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.stereotype.Service;

/**
 * 支付宝PC交易接口实现类
 *
 * @author WFT
 * @since 2024/4/14
 */
@Service
@RequiredArgsConstructor
public class ZfbPcTradeServiceImpl extends BaseTradeService {

    @Override
    public Scene getId() {
        return Scene.zfb_pc;
    }

    @Override
    public <T extends TradeBean> Object payment(T param) throws Exception {
        //  获取客户端配置
        ConfigBean configure = this.getConfigure(param.getClientId());
        //  创建支付宝交易对象
        AlipayTradePagePayRequest request = new AlipayTradePagePayRequest();
        //  设置支付回调地址和支付后返回的地址
        request.setNotifyUrl(String.format(configure.getPayNotifyPath(), configure.getAppId(), param.getId()));
        request.setReturnUrl(configure.getSuccessPath());
        //  支付参数
        JSONObject data = new JSONObject()
                .put("out_trade_no", param.getId())
                .put("total_amount", ZfbSDKUtil.getInstance().priceFormat(param.getTradePrice()))
                .put("subject", param.getTradeTitle())
                .put("product_code", "FAST_INSTANT_TRADE_PAY")
                .put("request_from_url", configure.getCancelPath())
                .put("extend_params", new JSONObject().put("sys_service_provider_id", configure.getSupplierId()));
        //  设置请求体
        request.setBizContent(data.toString());
        //  调用接口
        return ZfbSDKUtil.getInstance().execute(client -> client.pageExecute(request), configure);
    }

}
