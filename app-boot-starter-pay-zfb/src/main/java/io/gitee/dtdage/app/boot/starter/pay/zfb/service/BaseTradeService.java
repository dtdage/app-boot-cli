package io.gitee.dtdage.app.boot.starter.pay.zfb.service;

import com.alipay.api.request.AlipayFundTransUniTransferRequest;
import com.alipay.api.request.AlipayTradeRefundRequest;
import io.gitee.dtdage.app.boot.starter.pay.common.context.ExtractBean;
import io.gitee.dtdage.app.boot.starter.pay.common.context.RefundBean;
import io.gitee.dtdage.app.boot.starter.pay.zfb.context.ConfigBean;
import io.gitee.dtdage.app.boot.starter.pay.zfb.utils.ZfbSDKUtil;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * 支付宝交易接口
 *
 * @author WFT
 * @since 2024/4/14
 */
public abstract class BaseTradeService extends io.gitee.dtdage.app.boot.starter.pay.common.service.BaseTradeService<ConfigBean> implements ApplicationContextAware {

    private ConfigureService<?> configService;

    @Override
    protected ConfigBean getConfigure(String clientId) {
        return this.configService.getConfigure(clientId);
    }

    @Override
    public void setApplicationContext(ApplicationContext context) {
        this.configService = context.getBean(ConfigureService.class);
    }

    /**
     * @noinspection SpellCheckingInspection
     */
    @Override
    public <T extends ExtractBean> void extract(T param) throws Exception {
        //  获取客户端配置
        ConfigBean configure = this.getConfigure(param.getClientId());
        //  构建请求体
        AlipayFundTransUniTransferRequest request = new AlipayFundTransUniTransferRequest();
        //  收款人信息
        JSONObject payee = new JSONObject()
                /*
                    参与方的标识ID
                    当"identity_type=ALIPAY_USER_ID"时,填写支付宝用户UID
                    当"identity_type=ALIPAY_LOGON_ID"时,填写支付宝登录号
                 */
                .put("identity_type", "ALIPAY_LOGON_ID")
                /*
                    参与方的标识类型，目前支持如下枚举：
                    ALIPAY_USER_ID：支付宝会员的用户 ID，可通过 获取会员信息 能力获取。
                    ALIPAY_LOGON_ID：支付宝登录号，支持邮箱和手机号格式。
                 */
                .put("identity", param.getOutPayeeId())
                //  参与方真实姓名,如果非空,将校验收款支付宝账号姓名一致性
                .put("name", param.getOutPayeeName());
        //  数据
        JSONObject data = new JSONObject()
                .put("out_biz_no", param.getId())
                .put("trans_amount", ZfbSDKUtil.getInstance().priceFormat(param.getExtractPrice()))
                .put("product_code", "TRANS_ACCOUNT_NO_PWD")
                .put("biz_scene", "DIRECT_TRANSFER")
                .put("order_title", param.getExtractTitle())
                .put("remark", "")
                .put("payee_info", payee);
        //  设置业务参数
        request.setBizContent(data.toString());
        //  调用付款到余额接口
        ZfbSDKUtil.getInstance().execute(client -> client.certificateExecute(request), configure);
    }

    @Override
    public <T extends RefundBean> void refund(T param) throws Exception {
        //  获取客户端配置
        ConfigBean configure = this.getConfigure(param.getClientId());
        //  构建请求体
        AlipayTradeRefundRequest request = new AlipayTradeRefundRequest();
        JSONObject data = new JSONObject()
                .put("out_trade_no", param.getTradeNo())
                .put("out_request_no", param.getId())
                .put("refund_amount", ZfbSDKUtil.getInstance().priceFormat(param.getRefundPrice()));
        //  设置业务参数
        request.setBizContent(data.toString());
        //  调用退款接口
        ZfbSDKUtil.getInstance().execute(client -> client.certificateExecute(request), configure);
    }

}
